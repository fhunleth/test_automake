defmodule TestAutomake.Mixfile do
  use Mix.Project

  @target System.get_env("MIX_TARGET") || "rpi0"

  Mix.shell.info([:green, """
  Mix environment
    MIX_TARGET:   #{@target}
    MIX_ENV:      #{Mix.env}
  """, :reset])

  def project do
    [app: :test_automake,
     version: "0.1.0",
     elixir: "~> 1.4",

     compilers: Mix.compilers() ++ [:autoconf, :elixir_make],
     make_cwd: "src",
     autoconf_autoreconf: true,
     autoconf_cwd: "src",
     autoconf_configure_env: [
        {"ac_cv_lbl_unaligned_fail", "yes"}, {"ac_cv_func_mmap_fixed_mapped", "yes"}, {"ac_cv_func_memcmp_working", "yes"},
        {"ac_cv_have_decl_malloc", "yes"}, {"gl_cv_func_malloc_0_nonnull", "yes"}, {"ac_cv_func_malloc_0_nonnull", "yes"},
        {"ac_cv_func_calloc_0_nonnull", "yes"}, {"ac_cv_func_realloc_0_nonnull", "yes"}, {"lt_cv_sys_lib_search_path_spec", "\"\""},
        {"ac_cv_c_bigendian", "no"}, {"ac_cv_path_HELP2MAN", "\"\""}, {"CONFIG_SITE", "/dev/null"}],
     # Most of the paths below are wrong. I believe that they should be target paths
     autoconf_configure_args: ["--prefix", abspath("priv/_"),
                                "--target=arm-buildroot-linux-gnueabihf", "--host=arm-buildroot-linux-gnueabihf",
                                "--build=x86_64-pc-linux-gnu", "--exec-prefix", abspath("priv/_"),
                                "--sysconfdir", abspath("priv/_/etc"), "--localstatedir", abspath("priv/_/var"),
                                "--program-prefix=", "--disable-gtk-doc", "--disable-gtk-doc-html", "--disable-doc",
                                "--disable-docs", "--disable-documentation", "--with-xmlto=no", "--with-fop=no",
                                "--disable-dependency-tracking", "--enable-ipv6", "--disable-nls", "--disable-static",
                                "--enable-shared"],
     make_targets: ["install"],

     target: @target,
     archives: [nerves_bootstrap: "~> 0.6"],
     deps_path: "deps/#{@target}",
     build_path: "_build/#{@target}",
     lockfile: "mix.lock.#{@target}",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     aliases: aliases(@target),
     deps: deps()]
  end

  defp abspath(p), do: Path.join(File.cwd!(), p)

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application, do: application(@target)

  # Specify target specific application configurations
  # It is common that the application start function will start and supervise
  # applications which could cause the host to fail. Because of this, we only
  # invoke TestAutomake.start/2 when running on a target.
  def application("host") do
    [extra_applications: [:logger]]
  end
  def application(_target) do
    [mod: {TestAutomake.Application, []},
     extra_applications: [:logger]]
  end

  # Dependencies can be Hex packages:
  #
  #   {:my_dep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:my_dep, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  def deps do
    [{:nerves, "~> 0.7", runtime: false}] ++
    deps(@target)
  end

  # Specify target specific dependencies
  def deps("host"), do: []
  def deps(target) do
    [
      {:bootloader, "~> 0.1"},
      {:nerves_runtime, "~> 0.4"},
      {:hello_automake, path: "/home/fhunleth/nerves/make-experiments/hello_automake"},
      #{:hello_automake, git: "git@bitbucket.org:fhunleth/hello_autoconf.git", branch: "master"},
      {:elixir_make, "~> 0.4", runtime: false},
    ] ++ system(target)
  end

  def system("rpi"), do: [{:nerves_system_rpi, ">= 0.0.0", runtime: false}]
  def system("rpi0"), do: [{:nerves_system_rpi0, ">= 0.0.0", runtime: false}]
  def system("rpi2"), do: [{:nerves_system_rpi2, ">= 0.0.0", runtime: false}]
  def system("rpi3"), do: [{:nerves_system_rpi3, ">= 0.0.0", runtime: false}]
  def system("bbb"), do: [{:nerves_system_bbb, ">= 0.0.0", runtime: false}]
  def system("ev3"), do: [{:nerves_system_ev3, ">= 0.0.0", runtime: false}]
  def system("qemu_arm"), do: [{:nerves_system_qemu_arm, ">= 0.0.0", runtime: false}]
  def system("x86_64"), do: [{:nerves_system_x86_64, ">= 0.0.0", runtime: false}]
  def system(target), do: Mix.raise "Unknown MIX_TARGET: #{target}"

  # We do not invoke the Nerves Env when running on the Host
  def aliases("host"), do: []
  def aliases(_target) do
    ["deps.precompile": ["nerves.precompile", "deps.precompile"],
     "deps.loadpaths":  ["deps.loadpaths", "nerves.loadpaths"]]
  end

end
